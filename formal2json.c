/*
    formal2json.c -  Converts the Peterhouse formal menu to a json document.

    Copyright (C) 2020-21  Andrew Jenkins

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 dated June, 1991.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
    Apologies to whoever signs up to maintain this. I wrote it in C to
    practice my C, rather than because C was necessarily particularly
    appropriate for something needing to be updated frequently.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <libgen.h>

#include <poppler.h>
#include <glib.h>
#include "hallmenu_common.h"


#define MAX_COURSE_ITEMS 10


/*
    course_item is unique to formal hall. It is a struct that contains 
    an either-or option, usually the meat and possibly vege alternative.
*/

typedef struct _course_item {
    // sometimes there is an alternative on offer.
    menu_item item;
    menu_item alt;
} course_item;

typedef struct _formal_menu {
    char date[11]; // e.g. 2020-11-01 plus space for the null term.
    char* title;
    menu_item bread;
    int n_starters, n_mains, n_desserts;
    course_item starter[MAX_COURSE_ITEMS];
    course_item main[MAX_COURSE_ITEMS];
    course_item dessert[MAX_COURSE_ITEMS];
} formal_menu;

//TODO: dietary info.
struct di_key {
    char *abbr;
    char *def;
};

bool course_item_has_alt(course_item *i) {
    /* name will be null ptr if it doesn't have a name */
    return i->alt.name;
}

void json_write_course_item(FILE *f, course_item* i) {
    /* Item/alt is essentially a couple so have a list with
     * first item, then null.
     */
    fputs("[", f);
    json_write_menu_item(f, &i->item);
    fputs(", ", f);
    if (course_item_has_alt(i))
    	json_write_menu_item(f, &i->alt);
    else
	fputs("null", f);
    fputs("]", f);
}

void json_write_menu(FILE *f, formal_menu* menu) {
    fputs("{", f);
    json_write_string_k_v(f, "title", menu->title);
    fputs(",", f);
    json_write_key(f, "bread");
    json_write_menu_item(f, &menu->bread); 
    fputs(",\n  ", f);
    
    json_write_key(f, "starter");
    fputs("[", f);
    for (int i=0; i<menu->n_starters; i++) {
        if (i!=0) fputs(", ", f);
	json_write_course_item(f, &menu->starter[i]);
    }
    fputs("]", f);
    fputs(",\n  ", f);

    json_write_key(f, "main");
    fputs("[", f);
    for (int i=0; i<menu->n_mains; i++) {
        if (i!=0) fputs(", ", f);
	json_write_course_item(f, &menu->main[i]);
    }
    fputs("]", f);
    fputs(",\n  ", f);
    
    json_write_key(f, "dessert");
    fputs("[", f);
    for (int i=0; i<menu->n_desserts; i++) {
        if (i!=0) fputs(", ", f);
	json_write_course_item(f, &menu->dessert[i]);
    }
    fputs("]", f);
   
    fputs("}", f);
}

void json_write_menus_by_date(FILE *f, formal_menu** menus, size_t n) {
    /* it's a double pointer because that's how it's called */
    fputs("{", f);
    for (size_t i=0; i<n; i++) {
	if (i > 0) {fprintf(f, ",\n");}
        json_write_key(f, menus[i]->date);
	json_write_menu(f, menus[i]);
    }
    fputs("}",f );
}

int offset_if_alt(const char *input) {
    /* Alts are indicated by "A - " or "A [EN-DASH] "
       Returns zero if not alt or the offset of the end of indicator
       if this is an alt.*/
    int i=0;
    const char* ALT_INDICATOR_1 = "A -";
    const char* ALT_INDICATOR_2 = "A \xe2\x80\x93";

    do {
        if (input[i] == 'A') {
            if (!strncmp(&input[i], ALT_INDICATOR_1, 3))
                return i+3;
            if (!strncmp(&input[i], ALT_INDICATOR_2, 5))
                return i+5;
        } else if (input[i] != ' ') {
            return 0;
        }
    } while (input [++i]);
    
    return 0;
}

void pretty_print_menu(formal_menu *menu) {
    puts(menu->title);
    puts(menu->date);
    
    puts("");
    puts(menu->bread.name);
    puts("");
    
    for (int i=0; i<menu->n_starters; i++) {
        puts(menu->starter[i].item.name);
        if (course_item_has_alt(&menu->starter[i]))
            printf ("OR: %s\n", menu->starter[i].alt.name);
    }
    puts("");
    for (int i=0; i<menu->n_mains; i++) {
        puts(menu->main[i].item.name);
        if (course_item_has_alt(&menu->main[i]))
            printf ("OR: %s\n", menu->main[i].alt.name);
    }
    puts("");
    for (int i=0; i<menu->n_desserts; i++) {
        puts(menu->dessert[i].item.name);
        if (course_item_has_alt(&menu->dessert[i]))
            printf ("OR: %s\n", menu->dessert[i].alt.name);
    }
}

void menu_free(formal_menu *menu) {
    free(menu);
}


formal_menu *menu_from_page(struct page *page) {
    /*
        Extracts the menu information from `page` to a newly allocated 
        menu struct `menu`. The caller must free this struct with menu_free() 
    
        May fail due to no menu being present or to out-of-memory error
        but won't tell you which lol.
    */
    
    const char *COURSE_SEPARATOR = "~~O~~"; 

    if (!(page->n_paragraphs >= 10)) return NULL;
    formal_menu *menu = malloc(sizeof(formal_menu));
    if (!menu) return NULL;
    
    int i0 = -1;
    for (int i=0; i<page->n_paragraphs; i++) {
	/* the intention is that i0 will then point to the first nonempty paragraph */
    	if (!all_spaces(page->paragraphs[i])) {
		i0=i;	
		break;
	}
    }
    if (i0 == -1) {
	    /* ADJ LT2022: if we didn't find text on any of the paragraphs then give up */
	printf("All paragraphs empty");
	free(menu);
	return NULL;
    }

    if (!parse_date(page->paragraphs[i0], menu->date)) {
        printf("Failed to parse date %s\n", page->paragraphs[i0]);
	/* In Lent 2022 there appears to be an empty paragraph so this bit annoyingly triggers.*/
	menu->date[0] = 0; // Just in case
    }
    
    /* Look for days without a formal. */

    int sep_count = 0; // how many times does the course separator appear?
    int sep_indices[5] = {0}; // | starter | main | dessert | coffee | spare;
    // 5 or more separators WILL cause a segfault lol this is good design.
    
    char* p1 = NULL; // first non-empty paragraph
    
    /* The -2 is because in 2020 there were 2 'footer' paragraphs.
       i.e. the dietary info and the standard comment about contacting
       servery staff if you have special dietary requirements.
    */
    for (int i=i0+2; i < page->n_paragraphs-2; i++) {
        if (!strncmp(COURSE_SEPARATOR, page->paragraphs[i], 5)) {
            // 1+ because the first sep will be the pointer to the end of p1
            if (sep_count > 5) {
                puts("Too many separators in this meal, there may be a course missing.");
                break;
            }
            sep_indices[1+sep_count++] = i;
        }
        if (!p1 && !all_spaces(page->paragraphs[i])) { 
            p1 = page->paragraphs[i];
            sep_indices[0] = i;
            }
    }
            
    if (!sep_count) {
        printf("Assuming no formal %s (no course separators) [reason may be \"%s\"]\n", menu->date, p1 ? p1: "N/A");
        return NULL;
    } else if (sep_count < 2) {
        printf("Only one course separator was found; there may be problems.");
    }
    
    /* 
       Otherwise let's assume there IS a formal this day, and work out 
       what's on the menu for it. Bread is the first nonempty parargraph,
       usually followed by a gap before the starter.  
    */
    
    parse_menu_item(p1, &menu->bread);
    /*
    printf("(\"%s\", \"%s\")\n", menu->bread.name, menu->bread.dietary_indicators); 
    */

    course_item *cur;
    course_item *prev;
    int *counters[] = {&menu->n_starters, &menu->n_mains, &menu->n_desserts};
    int *counter;
    course_item *courses[] = {&menu->starter[0], &menu->main[0], &menu->dessert[0]};
    course_item *course;
    
    bool prev_was_alt;
    int altoffset;
    
    /* We do the same thing thrice but with different pointers. */
    
    for (int c=0; c<3; c++) {
        // for each of the three courses...
        counter = counters[c];
        course = courses[c];
        
        *counter = 0;

        for (int i=sep_indices[c]+1; i<sep_indices[c+1]; i++) {
            if (!all_spaces(page->paragraphs[i])) {
                if ((altoffset=offset_if_alt(page->paragraphs[i]))) {
                    /*printf("alt: %s\n", &page->paragraphs[i][altoffset]);*/
                    /* need to make sure we already have at least one menu
                       item since otherwise this will cause problems. */
                    if (!*counter) {
                        puts("Error: alt before regular item.");
                    } else {
                        parse_menu_item(&page->paragraphs[i][altoffset], &course[*counter-1].alt);
                        /*printf("main: %s, alt: %s\n", 
                                  menu->starter[menu->n_starters-1].item.name,
                                  menu->starter[menu->n_starters-1].alt.name);
                        */
                        prev_was_alt = true;
                    }
                }
                else if (*counter + 1 > MAX_COURSE_ITEMS) {
                    puts("Too many items on this course");
                    break;
                } else {
                    parse_menu_item(page->paragraphs[i], &course[(*counter)++].item);
                    cur = &course[*counter - 1];
                    cur->alt.name = NULL; // this is the way to check whether an alt exists.
                    
                    if (*counter >= 2) {
                        prev = &course[(*counter) - 2];

                        if (all_spaces(cur->item.name) /*&& all_spaces(prev->item.dietary_indicators)*/) {
                            menu_item *last = prev_was_alt ? &prev->alt : &prev->item;
                            /* if this is the case then assume that this is 
                               dietary info for the line above. */
                               printf("Assuming %s belonged to %s.\n", cur->item.dietary_indicators, last->name);
                               free(last->dietary_indicators);
                               free(cur->item.name);
                               last->dietary_indicators = cur->item.dietary_indicators;
                               (*counter)--;
                        } else if (is_lowercase(cur->item.name[0])) {
                            menu_item *last = prev_was_alt ? &prev->alt : &prev->item;
                            
                            if (all_spaces(last->dietary_indicators)) {
                            /* if this item starts lowercase AND the previous had no dietary info 
                               then assume it's a line continuation. */
                                printf("\"%s\" looks like a continuation of \"%s\"\n", cur->item.name, last->name);
                                space_join_replace(&last->name, cur->item.name);
                                //puts(last->name);
                                free(cur->item.name);
                                free(last->dietary_indicators);
                                last->dietary_indicators = cur->item.dietary_indicators;
                                (*counter)--;
                            }
                        }
                    }
                    prev_was_alt = false;
                }
            }
        }
    }

    menu->title = strip_copy(page->paragraphs[i0+1]);
    //puts(menu->title);
    
    return menu;
}


int main(int argc, char** argv) {
    char *outfile;
    char termname[16]; /* Michaelmas YYYY has 15 letters, plus one for null term */
    bool print_to_stdout = false;
    
    if (argc < 2 || argc > 3) {
        puts("usage: ./formal2json FILENAME [OUTFILE]\n"
             "    Using '-' for OUTFILE, or not providing an argument, will\n"
             "    cause the program to print to stdout.");
        return 1;
    }

    if (argc == 3) {
        outfile = argv[2];
        print_to_stdout = (strcmp(outfile, "-") == 0);
    } else {
        print_to_stdout = true;
    }

    char *abspath = realpath(argv[1], NULL);
    if (!abspath) {
        printf("Failed to determine absolute path of %s", argv[1]);
        return 1;
    }
    gchar *pdf_uri = g_filename_to_uri(abspath, NULL, NULL);
    if (!pdf_uri) {
        puts("Could not create uri.");
        free(abspath);
        return 1;
    }
    PopplerDocument *my_document =
        poppler_document_new_from_file(pdf_uri, NULL, NULL);

    if (!my_document) {
        printf("Could not open document.\n");
        g_free(pdf_uri);
        free(abspath);
        return 1;
    }

    if (!print_to_stdout) {
        puts("Document info:");
        print_document_metadata(my_document, "    ");
    }

    PopplerStructureElementIter *struct_elems =
        poppler_structure_element_iter_new(my_document);

    if (!struct_elems) {
        puts("Parsing cannot continue: document contains no structure elements.");
        return 1;
    }

    /* everything above here is more or less boilerplate */
    int n_pages = poppler_document_get_n_pages(my_document);
    struct page *pages = calloc(n_pages, sizeof(struct page));
    if (pages == NULL) {
        puts("Failed to allocate memory to page structs.");
        return 1;
    }

    PopplerStructureElementIter *tl = // the top-level document part
        poppler_structure_element_iter_get_child(struct_elems);

    structure_load_page_text(pages, tl);
    poppler_structure_element_iter_free(struct_elems);
    poppler_structure_element_iter_free(tl);
    
    formal_menu **menus = calloc(n_pages, sizeof(formal_menu*));
    if (!menus) {
        puts("Failed to allocate memory to menus");
        return 1;
    }
    
    int n_menus=0;
    for (int i=0; i<n_pages; i++) {
        menus[n_menus] = menu_from_page(&pages[i]);
        if (menus[n_menus]) n_menus++;
    }
    
    deduce_term_name(&termname[0], &menus[0]->date[0]);  
    printf("Term name: %s\n", termname);

    /*for (int i=0; i<n_menus; i++) {
    *    puts("====================");
    *    pretty_print_menu(menus[i]);
    *}
    */
    
    FILE *f = print_to_stdout ? stdout : fopen(outfile, "w");
    fputs("{ \"meta\" : {\n", f);

    json_write_string_k_v(f, "filename", basename(abspath));
    fputs(",\n", f);
    free(abspath);
    json_write_document_metadata(f, my_document);
    fputs("},\n", f);
    
    json_write_string_k_v(f, "term_name", &termname[0]);
    fputs(",\n", f);
    
    /* ************************************************
       Swap the following back in if it goes horribly wrong and you decide 
       it would be easier to just do it in Python
       ************************************************ */
    /* 
    fputs("\"paragraphs_by_page\" : [", f);
    json_write_paras_by_page(f, pages, n_pages);
    fputs("]}\n", f);
    */
    
    json_write_key(f, "menus_by_date");
    fputs("\n", f);
    json_write_menus_by_date(f, menus, n_menus);
    
    fputs("}\n", f);
    fclose(f);
    
    /* Only uncomment this if you are very boring and care about whether the lookup
     * table for PDFDocEncoding is working. */
    /*printf("PDFDocEncoding LUT hits: %d misses: %d\n", pdfdocencoding_lut_hits, pdfdocencoding_lut_misses);*/
    
    for (int i=0; i<n_pages; i++) {
        page_free_paragraphs(&pages[i]);
    }
    for (int i=0; i<n_menus; i++) {
        menu_free(menus[i]);
    }
    free(pages);
    free(menus);
    
    g_free(my_document);
    g_free(pdf_uri);
    return 0;
}
