# pdfdocenc.py - Registers a Python codec to convert to and from PDFDocEncoding,
# as specified in the PDF Specification v1.7, appendix D.2, PDFDocEncoding
#
# Copyright (C)  Andrew Jenkins 2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import codecs

# the Unicode code points for PDE code points between 0x17 and 0x20, exclusive.
# PDE points   0x18,   0x19,   0x1A,   0x1B    0x1C    0x1D    0x1E    0x1F
_pde_tab1 = [0x02D8, 0x02C7, 0x02C6, 0x02D9, 0x02DD, 0x02DB, 0x02DA, 0x02DC]

# the Unicode code points for PDE code points between 0x7F and 0xA1, exclusive.
# For some reason 0x9F is undefined
# PDE points   0x80,   0x81, ...
_pde_tab2 = [0x2022, 0x2020, 0x2021, 0x2026, 0x2014, 0x2013, 0x0192, 0x2044,
             0x2039, 0x203A, 0x2212, 0x2030, 0x201E, 0x201C, 0x201D, 0x2018,
             0x2019, 0x201A, 0x2122, 0xFB01, 0xFB02, 0x0141, 0x0152, 0x0160,
             0x0178, 0x017D, 0x0131, 0x0142, 0x0153, 0x0161, 0x017E, None,
             0x20AC]

encname = "pdfdocencoding"

def _unicode_codepoint_for_pde(pde_codepoint):
    # Returns None for those characters that are undefined as there is no
    # corresponding code point

    # most of these are undefined in pdfdocencoding
    if pde_codepoint <= 0x17:
        if pde_codepoint in [0x09, 0x0A, 0x0D]:
            # these are \t, \r, \n respectively.
            return pde_codepoint
        else:
            return None
    elif (pde_codepoint < 0x20):
        return _pde_tab1[pde_codepoint - 0x18]
    # all of the ascii printable chars are the same
    elif (pde_codepoint <= 0x7E):
        return pde_codepoint
    # Unicode DEL is undefined
    elif pde_codepoint == 0x7F:
        return None
    # the second set of difficult characters.
    elif pde_codepoint < 0xA1:
        return _pde_tab2[pde_codepoint - 0x80]
    # all subsequent code points are the same, except for 0xAD which is
    # undefined.
    elif pde_codepoint <= 0xFF:
        return pde_codepoint if pde_codepoint != 0xAD else None
    else:
        raise ValueError("Code point must be in range [0,255]")

def _pde_codepoint_for_unicode(unicode_codepoint):
    # Returns None for those characters that are undefined as there is no
    # corresponding code point

    # most of these are undefined in pdfdocencoding
    if unicode_codepoint <= 0x17:
        if unicode_codepoint in [0x09, 0x0A, 0x0D]:
            # these are \t, \r, \n respectively.
            return unicode_codepoint
        else:
            return None
    # all of the ascii printable chars are the same
    elif 0x20 <= unicode_codepoint <= 0x7E:
        return unicode_codepoint
    # and the higher valued ones
    elif 0xA1 <=unicode_codepoint <= 0xFF:
        return unicode_codepoint if unicode_codepoint != 0xAD else None

    # finally lookup in tables
    elif (unicode_codepoint in _pde_tab1):
        return 0x18 + _pde_tab1.index(unicode_codepoint)
    elif (unicode_codepoint in _pde_tab2):
        return 0x80 + _pde_tab2.index(unicode_codepoint)

    # and ultimately give up
    else:
        return None

_dec_lut = []
_enc_lut = {}
for _i in range(0x100):
    _cp = _unicode_codepoint_for_pde(_i)
    _dec_lut.append(_cp)
    if _cp is not None:
        # only add reverse lookup for codes that actually exist
        _enc_lut[_cp] = _i

def decode(bs, errors="ignore"):
    strchars = []
    for (i, b) in enumerate(bs):
        try:
            dval = _dec_lut[b]
            strchars.append(chr(dval))
        except (IndexError, TypeError):
            if errors == "strict":
                raise UnicodeDecodeError(encname, text, i, i+1,
                                         "value not in range(256)")
            elif errors == "replace":
                strchars.append("\uFFFD")

    return ("".join(strchars), len(bs))

def encode(text, errors="ignore"):
    bs = []
    for i, c in enumerate(text):
        try:
            bs.append(_enc_lut[ord(c)])
        except KeyError:
            if errors == "strict":
                raise UnicodeEncodeError(encname, text, i, i+1,
                                         "no code for this character")

    return (bytes(bs), len(text))

def lookup(s):
    if s == encname:
        return codecs.CodecInfo(encode, decode, name=encname)

codecs.register(lookup)
