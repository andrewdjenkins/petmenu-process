# petmenu.py - Reformat menu json to show the information for a given day.
#
# Copyright (C)  2020-22 Andrew Jenkins
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import datetime
import re
import time

# edit mich 2021: allow more than one space between entries. Hopefully
# this won't mess things up too much...
dietary_info_re = re.compile(" *([A-Z]{1,2} *)+ *$")
# one of the keys is sometimes separated by a comma rather than the em dash
dietary_info_key_re = re.compile("([A-Z]{1,2}) *[,\u2013] +([A-Za-z ]+)(,|$)")

#ET2022: add question mark after brunch as it's occasionally not present.
brunch_info_re = re.compile("saturday (?:brunch )?(\\d+)\\w+ (\\w{3})\\w* *(\\d+)?")

def flatten_lines(s):
    return " ".join([l.strip() for l in s.split("\n")])

def split_dietary_info(s):
    """Turns e.g. 'Vegan Meatballs VV G ' into ("Vegan Meatballs",
    ["VV", "G"]) """

    info_substring = dietary_info_re.search(s)
    if info_substring:
        info_substring_s = info_substring[0]
        info_symbols = [u.strip() for u in info_substring_s.split(" ")]
        return (flatten_lines(s[:info_substring.start()].strip()), [u for u in info_symbols if u!=""])
    else:
        return (flatten_lines(s), [])

def _parse_sides(s):
    # sides are separated by two newlines.
    s_stripped = "\n".join([l.strip() for l in s.split("\n")])
    sides = [split_dietary_info(p) for p in s_stripped.split("\n\n")]
    pretty_sides = [(flatten_lines(si), di) for si, di in sides]

    return [ (si, di) for (si, di) in pretty_sides if si.strip() != ""]

def _parse_jacket_spud(s):
    # I suppose given the college's reputation for potatoes, I shouldn't complain
    # about having to parse this.

    # It seems to be roughly one entry per line with occasional linebreaks so something
    # like what was done with the formal hall entries would make sense.
    lines = [l.strip() for l in s.split("\n")]
    #for i, l in enumerate(lines):
    #    # candidate for continuation
    #    if not l[0].isupper()
    spuds = [split_dietary_info(l) for l in lines]
    pretty_sides = [(flatten_lines(si), di) for si, di in spuds]

    return [ (si, di) for (si, di) in pretty_sides if si.strip() != ""]

_months = ["jan", "feb", "mar", "apr", "may", "jun",
           "jul", "aug", "sep", "oct", "nov", "dec"]

_days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]

def true_date(fancy_date, year):
    """For a fancy date of the format 'Monday 1st September', returns (D,M,Y)"""
    fdate_parts = [p.strip() for p in fancy_date.split(" ")]
    fdate_parts = [f.lower() for f in fdate_parts if f != ""]

    if len(fdate_parts) != 3:
        raise ValueError("Could not parse date '{}'.".format(fancy_date))

    dow = _days.index(fdate_parts[0][:3])
    dom = int("".join([c for c in fdate_parts[1] if c.isdigit()]))
    mon = _months.index(fdate_parts[2][:3]) + 1

    tentative_date = datetime.date(year, mon, dom)
    if tentative_date.weekday() != dow:
        # this means either there's a typo or the year is wrong. Try one year
        # either side of that given.
        tentative_date = datetime.date(year+1, mon, dom)
        if tentative_date.weekday() != dow:
            tentative_date = datetime.date(year-1, mon, dom)
            if tentative_date.weekday() != dow:
                raise ValueError("The date {} does not occur in within a year of {}".format(fancy_date, year))

    return tentative_date

def true_date_from_lazy(day_date, month, year):
    # For when we know what month it is and the text says e.g. "thursday 28th"

    # Note to future maintainers: this is a horribly inelegant way to do this

    long_text = "{} {}".format(day_date, _months[month-1])
    return true_date(long_text, year)


class MenuRetabler:

    def __init__(self, year):
        self._table_by_date = {}
        self.year = year
        self._dietary_requirements = {}
        self.breakfast = []

        # The last value of month the system has seen on any servery table.
        self._month = None

    def _row_type_nice(self, s):
        return s if (s.endswith("s") or s=="soup" or s=="potato") else s + "s"

    def row_type_for_str(self, s):
        sl = s.lower()

        # Lent21 adds 'Hot Sweet' row.
        # Mich 21 adds 'Jacket Potatoes' in lunch only
        for row_type in ["main", "dessert", "salad", "sides", "soup", "potato"]:
            if row_type in sl:
                return self._row_type_nice(row_type)

        if "hot sweet" in sl:
            return "desserts"
        else:
            raise ValueError("Could not determine row type of {}".format(sl))

    def parse_date_row(self, row):
        dates = []
        month = None
        # The first itme of the row is not a date
        for cell_text in row[1:]:
            date_text = cell_text.strip()
            try:
                date = true_date(date_text, self.year)
                self._month = month = date.month
                date = date.isoformat()
            except ValueError:
                # LT2022: Fails to parse date where it doesn't specify the month
                # If we've already seen a month on this row of headings then this will succeed.

                # ET2022: FFS, now they've got one where they don't even specify the date.
                if month is None:
                    # The intention is that the month thing should function
                    # EXACTLY as before except when there is no month mentioned
                    # on the first entry on a given page. In this case only, the
                    # system will recall the last month it read on any page,
                    # which may be erroneous
                    if self._month:
                        month = self._month
                    else:
                        # this will be the case if we haven't seen a month yet
                        # in the whole document
                        # just don't produce an entry for this day.
                        dates.append(None)
                        continue
                try:
                    # Once we've remembered the month, try again.
                    date = true_date_from_lazy(date_text, month, self.year).isoformat()
                except ValueError:
                    dates.append(None)
                    continue
            dates.append(date)
        return dates

    def load_servery_table(self, t):

        ttype = (t[0][0]).strip().lower()
        if "dinner" in ttype:
            ttype = "dinner"
        elif "lunch" in ttype:
            ttype = "lunch"
        else:
            ttype = "misc"

        # LT2025: Some of the tables are now broken over multiple pages
        # A suitable condition to identify this may be if none of the dates on
        # the top row can be parsed

        # top row is assumed to be a header so only get type for rows 1 onwards
        rowtypes = [self.row_type_for_str((r[0])) for r in t[1:]]
        n_rows = len(t)
        month = None
        if n_rows < 3:
            print(f"WARNING: Table only has {n_rows} row(s). This may be a sign it's broken across pages.")
            print(f"         Table is: {t}")

        dates = self.parse_date_row(t[0])
        for colnum in range(1, len(t[0])):
            date = dates[colnum-1] # because there's no date for the first column
            if date is None:
                print("WARNING: Can't parse date '{}'. There will be no data for this day.".format(t[0][colnum]))
                continue

            this_day_record = self._table_by_date.setdefault(date, {})
            for rownum in range(1, n_rows):
                rt = rowtypes[rownum - 1] # because there's no type for the top row

                this_cell = (t[rownum][colnum])
                # bin any empty ones.
                if this_cell.strip() == "":
                    continue

                # Mains and sides are stored separately for lunch and dinner.
                if rt in ["mains", "sides"]:
                    dest0 = this_day_record.setdefault(ttype, {})
                else:
                    dest0 = this_day_record

                # these ones take the form of lists.
                if rt in ["mains", "desserts", "salads"]:
                    dest1 = dest0.setdefault(rt, [])
                    dest1.append(split_dietary_info(this_cell.strip()))
                elif rt in ["sides"]:
                    dest0[rt] = _parse_sides(this_cell)
                elif rt in ["potato"]:
                    dest0[rt] = _parse_jacket_spud(this_cell)
                else:
                    dest0[rt] = split_dietary_info(this_cell.strip())

    def load_servery_tables(self, tables):

        # LT2025: Some of the tables are broken over multiple pages.
        # Do a check for this here - if we fail to parse the first line, then
        # try merging it with the previous table.

        orphans = []

        for i, tab in enumerate(tables):
            dates = self.parse_date_row(tab[0])
            if all([(d is None) for d in dates]):
                print(f"Orphaned table {i}")
                orphans.append(i)

        # Reset the state to make sure that load_servery_table doesn't get
        # confused by old month names.
        self._month = None

        # Now merge orphaned tables with the previous one.
        for orphan_idx in reversed(orphans):
            # Work backwards as this leaves earlier indices unchanged
            # even after we delete elements from the table.

            orphan_table = tables[orphan_idx]
            prev_table = tables[orphan_idx-1]

            # If there is an empty first box on the rollover table, assume
            # this row is a continuation of the row itself above rather than
            # a new 'logical' row of the table
            if orphan_table[0][0].strip() == "":
                print("NOTE: Appending row of rollover table to previous")
                print(f"      row is: {orphan_table[0]}")
                for i in range(len(orphan_table[0])):
                    prev_table[-1][i] += "\n" + orphan_table[0][i]
                del orphan_table[0]

            prev_table += orphan_table
            print(f"Merged table {orphan_idx} with previous")
            del tables[orphan_idx]

        for tab in tables:
            self.load_servery_table(tab)

    def dietary_requirement_longhand(self, k):
        return self._dietary_requirements[k]

    def load_dietary_requirement_key(self, l):
        matches = dietary_info_key_re.findall(l)
        if matches:
            # print(matches)
            for k, v, _ in matches:
                if k not in self._dietary_requirements:
                    # print(k, v)
                    self._dietary_requirements[k] = v

    def load_dietary_requirement_keys_from_paras(self, paras):
        for p in paras:
            if p.strip() != "":
                self.load_dietary_requirement_key((p.strip()))

    def term_name(self):
        dates = list(self._table_by_date.keys())
        dates.sort()
        first_date = dates[0]
        # similar thing done in C code in formal2json.
        y, m, d = (int(i) for i in first_date.split("-"))
        if 9 <= m < 12: termname = "Michaelmas"
        elif m == 12 or m < 3: termname = "Lent"
        elif 3 <= m < 7: termname = "Easter"
        else: termname = "Summer"

        return "{} {}".format(termname, y)


    def to_dict(self):
        return {"di_legend" : self._dietary_requirements,
                "term_name" : self.term_name(),
                "menus_by_date" : self._table_by_date,
                "breakfast" : self.breakfast}

    def load_brunch_tables(self, tables):

        for table in tables:
            tdate = (table[0][0]).lower().strip()
            m = brunch_info_re.match(tdate)
            if not m:
                raise ValueError("Brunch date regex did not match string {}".format(repr(tdate)))
            dom = int(m[1])
            mon = _months.index(m[2]) + 1
            year = m[3] if m[3] else self.year

            date = "{}-{:02d}-{:02d}".format(year, mon, dom)

            this_day = self._table_by_date.setdefault(date, {})

            # this index is where the actual items are

            s = table[1][0]
            # MOST items are separated by two newlines.
            s_stripped = "\n".join([l.strip() for l in s.split("\n")])
            s_stripped = [i.strip() for i in s_stripped.split("\n\n") if i.strip() != ""]

            i_special = None
            # now look for a special (because separated only by single newline)
            for i, s in enumerate(s_stripped):
                if s.lower().startswith("brunch special"):
                    # first line is "brunch special" so cut that line out
                    brunch_specials = [split_dietary_info(l) for l in
                                            s.split("\n")[1:]]
                    # Index at which the first of the subsequent specials occurs.
                    i_special = i
                    del s_stripped[i]
                    break
            else:
                brunch_specials = []

            couples = [split_dietary_info(p) for p in s_stripped]
            couples = [(flatten_lines(si), di) for si, di in couples]
            brunch = [ (si, di) for (si, di) in couples if si.strip() != ""]
            # Now move over any specials that came after the first one.
            if i_special is not None:
                brunch_specials.extend(brunch[i_special:])
                brunch = brunch[:i_special]

            this_day["brunch"] = {"regular" : brunch}
            if len(brunch_specials) > 0 :
                this_day["brunch"]["specials"] = brunch_specials

    def load_breakfast_tables(self, data):
        # Breakfast does not vary day-to-day, so can include "breakfast"
        # alongside menus_by_date

        # this is nice because it has exactly the same format as the sides list
        alltext = data[0][1][0]
        self.breakfast = _parse_sides(alltext)

    def auto_load_tables(self, tables):
        # first row of first col of first table
        menutitle = tables[0][0][0].lower()
        if "brunch" in menutitle:
            print("Brunch menu")
            self.load_brunch_tables(tables)
        elif "breakfast" in menutitle:
            print("Breakfast menu")
            self.load_breakfast_tables(tables)
        else:
            print("Lunch & Dinner menu")
            self.load_servery_tables(tables)

    def auto_load_json(self, jdata):
        self.auto_load_tables(jdata["tables"])
        self.load_dietary_requirement_keys_from_paras(jdata["paragraphs"])

    @classmethod
    def from_source_json(cls, jdata):
        year = time.gmtime(jdata["meta"]["date"]).tm_year
        rt = cls(year)
        rt.auto_load_json(jdata)
        return rt

def fix_source_json(jdata):
    return MenuRetabler.from_source_json(jdata).to_dict()

def multi_source_json(initial, *additional):
    mt = MenuRetabler.from_source_json(initial)
    for jdata in additional:
        mt.auto_load_json(jdata)

    return mt.to_dict()


# === Entry points / scripts === #

def _main_reformat(infiles, outf):
    """Reformat takes source json files and gives an output json file."""
    import json

    jdata = []
    for inf in infiles:
        with open(inf, encoding="utf-8") as f:
            jdata.append(json.load(f))

    j_out = multi_source_json(*jdata)

    with open(outf, "w", encoding="utf-8") as f:
        json.dump(j_out, f, sort_keys=True)


if __name__ == "__main__":
    from sys import argv, stderr
    import json

    command = argv[1].lower()
    if command == "reformat":
        inf = argv[2:-1]
        outf = argv[-1]
        _main_reformat(inf, outf)

    else:
        print("Usage: petmenu reformat INFILE1 [[INFILE2] ...] OUTFILE")
        exit(1)

