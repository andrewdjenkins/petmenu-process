/*
    hallmenu_common.c -  Functions useful to both formal and regular menus.

    Copyright (C) 2020-21  Andrew Jenkins

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 dated June, 1991.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* boring poppler stuff -- ignore this most likely. */


#include "hallmenu_common.h"

bool system_is_big_endian(void){
    /*
    This is needed in order for pdfdocencoding to work
    https://stackoverflow.com/questions/1001307/detecting-endianness-programmatically-in-a-c-program#1001373
    */
    union {
        uint32_t i;
        uint8_t c[4];
    } u = {0xAABBCCDD};

    return u.c[0] == 0xAA;
}

void print_poppler_info(void) {
    char *backend;
    switch (poppler_get_backend()) {
        case POPPLER_BACKEND_SPLASH:
            backend = "Splash";
            break;
        case POPPLER_BACKEND_CAIRO:
            backend = "Cairo";
            break;
        case POPPLER_BACKEND_UNKNOWN:
            backend = "unknown";
            break;
        default:
            perror("poppler_get_backend() returned unexpected value");
            backend = "unknown";
    }
    printf("Using Poppler %s with %s backend.\n", poppler_get_version(), backend);
}

void print_document_metadata(PopplerDocument *d, const char *indent) {

    gchar *title, *subject, *author, *creator, *producer;
    title = poppler_document_get_title(d);
    subject = poppler_document_get_subject(d);
    author = poppler_document_get_author(d);
    creator = poppler_document_get_creator(d);
    producer = poppler_document_get_producer(d);

    printf("%sTitle: %s\n%sSubject: %s\n%sAuthor: %s\n%sCreator: %s\n%sProducer: %s\n\n",
           indent, title ? title : "None",
           indent, subject ? subject : "None",
           indent, author ? author : "None",
           indent, creator ? creator : "None",
           indent, producer ? producer : "None");

    g_free(title);
    g_free(subject);
    g_free(author);
    g_free(creator);
    g_free(producer);
}

/* PAGE STRUCT THINGS */
int page_add_paragraph (struct page *page, gchar *s) {
    /* Append a paragraph to the page struct and return 1 on success,
       0 on failure. */
       
    if (page->n_paragraphs == MAX_PARAGRAPHS)
        return 0;
    page->paragraphs[(page->n_paragraphs)++] = s;
    return 1;
}

void page_free_paragraphs (struct page *page) {
    for (int i=0; i<page->n_paragraphs; i++) {
        g_free(page->paragraphs[i]);
    }
    page->n_paragraphs = 0;
}

void structure_load_page_text (struct page *pages, PopplerStructureElementIter *iter) {
    /* Saves all of the paragraphs from iter to a string */
    
    int pagenum;
    gchar *elemtext, *elemtext2;
    
    do {
        PopplerStructureElement *elem =
            poppler_structure_element_iter_get_element(iter);
        PopplerStructureElementKind kind =
            poppler_structure_element_get_kind(elem);

        if (kind == POPPLER_STRUCTURE_ELEMENT_PARAGRAPH) {

            pagenum = poppler_structure_element_get_page(elem); 
            if (pagenum < 0) {
                puts("Page number unknown.");
                continue;
            }
            
            elemtext = poppler_structure_element_get_text(elem,
                        POPPLER_STRUCTURE_GET_TEXT_RECURSIVE);
	    /* this CAN be done in place but this way is probably more versatile */
	    elemtext2 = pdfdocencoding_workaround_copy(elemtext);
	    /*printf("%s -> %s\n", elemtext, elemtext2);*/
	    g_free(elemtext);
	    elemtext = elemtext2;
	    if (!elemtext) {
		puts("Re-encoding failed.");
		continue;
	    }

            if (!page_add_paragraph(&(pages[pagenum]), elemtext)) {
                puts("structure_load_page_text: Failed to add paragraph reference to page");
                g_free(elemtext);
            }
        }
    } while (poppler_structure_element_iter_next (iter));
}

/******
 * STRING STUFF
 ******/

bool chk_codepoints(uint32_t *input, size_t n_input) {
    for (size_t i=0; i<n_input; i++) {
        if (input[i] > 0x10FFFF) {
            fprintf(stderr, "chk_codepoints: invalid value %X at position %zu/%zu\n", input[i], i, n_input);
            return false;
        }
    }
    return true;
}

bool all_spaces(const char *input) {
    /*
        True if the contents of the paragraph are all space  
    */
    int i=0;
    while (input[i++]) {
        if (input[i-1] != ' ') return false;  
    }
    
    return true;
}

bool is_lowercase(char x) {
    return x >= 'a' && x <= 'z';
}

char* strip_copy(const char *input) {
    /*
        Returns a malloc'd copy of input but with leading and trailing
        spaces removed.    
    */
    
    /*
        The gist is that we don't copy any spaces to the output until
        we know there's something following them. So when we see a 
        space, we just increment the space counter, and add them all in
        one go when we find the next character.
    */
    
    int n = strlen(input);
    int j=0;
    bool started = false;
    int spaces = 0;
    char *out = calloc(n+1, sizeof(char));
    if (!out) return NULL;
    
    for (int i=0; i<n; i++) {
        if (input[i] != ' ') {
            started = true;
            for (; spaces>0; spaces--){
                out[j++] = ' ';
            }
            out[j++] = input[i];
        } else if (started) {
            spaces++;
        }
    }
    // Once we've reached the end of the string, null-terminate it.
    out[j] = 0;
    return out;
}

char* strip_ncopy(const char *input, size_t n) {
    /*
        Returns a malloc'd copy of input but with leading and trailing
        spaces removed. Here n DOES NOT include the null terminator    
    */
    
    /*
        The gist is that we don't copy any spaces to the output until
        we know there's something following them. So when we see a 
        space, we just increment the space counter, and add them all in
        one go when we find the next character.
    */
    
    int j=0;
    bool started = false;
    int spaces = 0;
    char *out = calloc(n+1, sizeof(char));
    if (!out) return NULL;
    
    for (size_t i=0; i<n; i++) {
        if (input[i] != ' ') {
            started = true;
            if (input[i] == 0) {
                j++;
                break;
            }
            for (; spaces>0; spaces--){
                out[j++] = ' ';
            }
            out[j++] = input[i];
        } else if (started) {
            spaces++;
        }
    }
    // Once we've reached the end of the string, null-terminate it.
    out[j] = 0;
    return out;
}

char* space_join_replace(char** dest, char* to_add) {
    /* dest must be a malloc'd string which we will realloc and then 
    append to_add after a space. */
    size_t l1 = strlen(*dest);
    size_t l2 = strlen(to_add);
    (*dest) = realloc((*dest), (l1 + l2 + 2) * sizeof(char));
    if (!dest) return NULL;
 
    snprintf(*dest + l1 * sizeof(char), l2+2, " %s", to_add);
    
    return *dest;
}

int parse_date(char *date, char *dest) {
    /* Where `date` is a date of the form "Tuesday 2nd January 2021"
       and `dest` is a buffer at least 11 chars large. 
    */
    const char* months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    char cur;
    int word=0;
    int i=0;
    bool in_ws = true;
    int mon=0;
    int day=0;
    int year=0;
    
    do {
        cur = date[i];
        if (in_ws && cur != ' ') {
            // we're at the start of a word.
            in_ws = false;
            word += 1;
            
            switch (word) {
                case 1:
                    continue;
                case 2:
                    // this will go up to the 'th' or 'nd'
                    day = strtol(&date[i], NULL, 10);
                    //printf("day: %d\n", day);
                    break;
                case 3:
                    for (int j=0; j<12; j++) {
                        if (!strncmp(&date[i], months[j], 3)) {
                            mon = j+1;
                            break;
                        }
                    }
                    break;
                case 4:
                    // this will go to the end.
                    year = strtol(&date[i], NULL, 10);
                    break;
                    
                default:
                    break;
            }
        }
        
        if (!in_ws && cur == ' ') {
            in_ws = true;
        }
        i+=1;
        
    } while (cur != 0);
    
    if (day && mon && year) {
        snprintf(dest, 11, "%04d-%02d-%02d", year, mon, day);
        //puts(dest);
        return 1;
    }
    
    return 0;
}

void deduce_term_name(char *dest, const char *date) {
    /* i.e. 2021-01-10 -> Lent 2021. Give it the start date. Cuttofs Sep-Nov: Mich, Dec-Feb Lent,
     * Mar-Jun Easter, anything else Summer.*/
    
    int year = strtol(date, NULL, 10);
    int mon  = strtol(&date[5], NULL, 10);

    char *termname;

    if      (mon >=  9 && mon < 12) termname = "Michaelmas";
    else if (mon == 12 || mon <  3) termname = "Lent";
    else if (mon >=  3 && mon <  7) termname = "Easter";
    else termname = "Summer";

    snprintf(dest, 16, "%s %d", termname, year);
    dest[16] = 0;
}

/****** 
 * ENCODING STUFF 
 ******/
int pdfdocencoding_lut_hits = 0;
int pdfdocencoding_lut_misses = 0;
void pdfdocencoding_encode(uint32_t *input, size_t n_input,
                          char *output, size_t n_output) {
    
    // lookup table to supposedly speed up inputs.
    static int enc_lut[65535] = {0};

    // whichever is least of the number in input and number in output    
    size_t lim = n_input < n_output ? n_input : n_output;
    bool found;
    for (size_t i=0; i<lim; i++) {
        found = false;
        if (!input[i]) {
            // we've reached the end of the string.
            output[i] = 0;
            break;
        }

        if (enc_lut[input[i]]) {
            pdfdocencoding_lut_hits++;
            if (enc_lut[input[i]] == -1) {
                // cache hit but no code for char.
                puts("pdfdocencoding_encode: no code for char.");
            } else {
                // cache hit. use it.
                output[i] = enc_lut[input[i]];
            }
        } else {
            // cache miss
            pdfdocencoding_lut_misses++;
            for (int j=0; j<256; j++) {
                if (input[i] == pdfDocEncoding[j]) {
                    output[i] = j;
                    enc_lut[input[i]] = j;
                    //printf("%c -> %c\n", input[i], (char) j);
                    found = true;
                    break;
                } 
                /*
                else {
                    printf("%lu != %i\n", input[i], j);
                }*/
            }
            if (!found) {
                enc_lut[input[i]] = -1;
                puts("pdfdocencoding_encode: no code for char.");
            }
        }
    }
}

char *pdfdocencoding_workaround_copy(char* s) {
    /* Poppler encounters some text which it thinks is in PDFDocEncoding,
     * which is actually in UTF-8. It converts (decodes to code points, encodes to
     * UTF-8) it to what it thinks is UTF-8 in order to return it. To get the 
     * original string back, we need to go from pdf to code points, then
     * encode these to PDFDocEncoding (which will leave us with something that is
     * actually UTF-8)
    */
    char *s2;
    size_t input_chars, input_chars_left, output_bytes_left, output_codepoints_allocated, rslt;
    /* the 2 is for iconv to iterate with */
    uint32_t *codepoints, *codepoints2;

    s2 = s; // create a duplicate pointer 
    input_chars = strlen(s);
    input_chars_left = input_chars;
    
    gchar* output_buffer = g_malloc_n(input_chars +1, sizeof(char));
    if (output_buffer == NULL) {
	puts("Failed to allocate memory for PDE output");
	return NULL;
    }
    
    /* 
        iconv works by the byte so tell it the max number of 
        bytes it can have, not the max number of characters.
    */
    output_bytes_left = input_chars * sizeof(uint32_t);
    output_codepoints_allocated = input_chars_left + 1;

    /* 
         we know we won't overflow because PDE is 1 byte per character
         and UTF32 is 1 quad per character
    */
    codepoints = calloc(output_codepoints_allocated, sizeof(uint32_t));
    codepoints2 = codepoints;
            
    if (codepoints == NULL) {
        puts("pdfdocencoding_convert_to_new: Failed to allocate memory for UTF-32");
	g_free(output_buffer);
        return NULL;
    } 
    /*
     * Using wchar_t means the system endianness is matched... but apparently only on Linux.
     * https://stackoverflow.com/questions/62032729/using-iconv-with-wchar-t-on-linux#62033260
     * For it to work on Windows the encoding needs to be set explicitly based on the system
     * endianness.
    */
    errno = 0;
    char *use_encoding = system_is_big_endian() ? "UTF-32BE" : "UTF-32LE";
    iconv_t cd = iconv_open(use_encoding, "UTF-8");

    if (errno) {
        perror("Failed to create iconv conversion descriptor");
	free(codepoints);
	g_free(output_buffer);
        return NULL;
    }
    rslt = iconv(cd,
                &s2, &input_chars_left,
                (char**) &codepoints2, &output_bytes_left);
    iconv_close(cd);
    if (rslt == (size_t) -1) {
        /* Error in iconv */
        perror("pdfdocencoding_workaround_copy (iconv call)");
    }
    if (input_chars_left) {
        printf("Didn't convert all input chars; %zu/%zu left\n", input_chars_left, input_chars);
    }

    if (output_bytes_left % sizeof(uint32_t) != 0) {
        fputs("iconv output a number of bytes which is not divisible by sizeof(uint32_t). Errors may ensue.", stderr);
    }

    /* The sizeof in the below is because output_bytes_left refers to the number of bytes whereas allocated is number
        of characters left in the string*/
    if (!chk_codepoints(codepoints, output_codepoints_allocated - output_bytes_left / sizeof(uint32_t)))
        puts("Not all codepoints valid. If output is garbled, try changing the encoding in source code.");

    pdfdocencoding_encode(codepoints, input_chars, output_buffer, input_chars);
    output_buffer[input_chars] = 0;
    free(codepoints);
    return output_buffer;
}

/*****
 * JSON STUFF
 ****/
gchar *json_str_encode(const gchar *s) {
    /*  
        When we write out to json we need to encode certain chars like
        newlines and brackets. This function does so into a freshly 
        allocated string.
    */
    /*
        Do one pass to count the space we'll need for its encoding.
        Start with n=1 for null terminate.
    */
    int n = 1; // num output chars
    int i = 0; // num input chars
    int j = 0;
    gchar c;
    do {
        c = s[i++];
        switch (c) {
            case '\n':
                n += 2;
                break;
            case '\b':
                n += 2;
                break;
            case '\f':
                n += 2;
                break;
            case '\r':
                n += 2;
                break;
            case '\t':
                n += 2;
                break;
            case '\\':
                n += 2;
                break;
            case '/':
                n += 2;
                break;
            case '"':
                n += 2;
                break;
            default:
                n+=1;
        }
    } while (c);
    gchar *out = malloc(n * sizeof(gchar));
    if (!out) return NULL;
    out[n-1] = 0;

    i = 0; // input idx
    j = 0; // output idx.
    /* Now write into the destination string. */
    do {
        c = s[i++];
        switch (c) {
            case '\n':
                out[j++]='\\';
                out[j++]='n';
                break;
            case '\b':
                out[j++]='\\';
                out[j++]='b';
                break;
            case '\f':
                out[j++]='\\';
                out[j++]='f';
                break;
            case '\r':
                out[j++]='\\';
                out[j++]='r';
                break;
            case '\t':
                out[j++]='\\';
                out[j++]='t';
                break;
            case '\\':
                out[j++]='\\';
                out[j++]='\\';
                break;
            case '/':
                out[j++]='\\';
                out[j++]='/';
                break;
            case '"':
                out[j++]='\\';
                out[j++]='"';
                break;
            default:
                out[j++] = c;
        }
    } while (c);
    return out;
}

/* write $key : to file. */
void json_write_key(FILE *f, char *k) {
    char* k_enc = json_str_encode(k);
    fprintf(f, "\"%s\": ", k_enc);
    free(k_enc);
}

void json_write_string_value(FILE *f, char *v) {
    char* v_enc = json_str_encode(v);
    fprintf(f, "\"%s\"", v_enc);
    free(v_enc);
}

/* Write "$key" : "$value" to the file, json encoding both. */
void json_write_string_k_v(FILE *f, char *k, char*v) {
    if (v != NULL) {
        char *k_enc, *v_enc;
        k_enc = json_str_encode(k);
        v_enc = json_str_encode(v);

        fprintf(f, "\"%s\": \"%s\"", k_enc, v_enc);
        free(k_enc);
        free(v_enc);
    } else {
        json_write_null_k_v(f, k);
    }
}

/* Write "$key" : null to the file, json encoding the key.. */
void json_write_null_k_v(FILE *f, char *k) {
    char *k_enc;
    k_enc = json_str_encode(k);

    fprintf(f, "\"%s\": null", k_enc);
    free(k_enc);
}

void json_write_long_k_v(FILE *f, char*k, long i) {
    json_write_key(f, k);
    fprintf(f, "%ld", i);
}

/* this function is boring and self-explanatory. */
void json_write_document_metadata(FILE *f, PopplerDocument *d) {

    gchar *t;
    t = poppler_document_get_title(d);
    json_write_string_k_v(f, "title", t);
    fputs(",\n", f);
    g_free(t);

    t = poppler_document_get_subject(d);
    json_write_string_k_v(f, "subject", t);
    fputs(",\n", f);
    g_free(t);

    time_t date = poppler_document_get_creation_date(d);
    if (date < 0) date = poppler_document_get_modification_date(d);
    if (date > 0) {
        json_write_long_k_v(f, "date", date);
    } else {
        json_write_null_k_v(f, "date");
    }
    fputs(",\n", f);

    t = poppler_document_get_author(d);
    json_write_string_k_v(f, "author", t);
    fputs(",\n", f);
    g_free(t);

    t = poppler_document_get_creator(d);
    json_write_string_k_v(f, "creator", t);
    fputs(",\n", f);
    g_free(t);

    t = poppler_document_get_producer(d);
    json_write_string_k_v(f, "producer", t);
    g_free(t);
}

void json_write_dietary_list(FILE *f, char *l) {
    char *enc = json_str_encode(l);
    size_t len = strlen(enc);
    bool in_item = false;
    bool in_space = true;
    fputs("[", f);
    for (size_t i=0; i<len; i++) {
        if (enc[i] == ' ') {
	    if (!in_space) {
	        fputs("\", ", f);
	    }
	    in_space = true;
	    in_item = false;
	} else {
	    if (in_space) {
		fputs("\"", f);
		in_space = false;
	    }
	    fputc(enc[i], f);
	    in_item = true;
	}
    }
    if (in_item) fputc('"', f);
    fputs("]",f );
}

/***** 
 * MENU ITEM STUFF 
 *****/
int parse_menu_item(char *s, menu_item *item) {
    /* Separates the string s out into the item and dietary info within
       menu_item. Note that this allocates NEW strings.
       
       The dietary info is taken to be any all-caps words of three 
       characters or less at the end of the item.
    */
    
    int n = strlen(s);
    int this_abbr_len = 0;
    int i=n-1;
    /* Iterate backwards from the end of the string to find abbreviations. */
    //puts(s);
    for (; i>=0; i--) {
        if (s[i] == ' ') {
            this_abbr_len = 0;
            //puts("space");
        }
        else if ((s[i] >= 'A' && s[i] <= 'Z')) {
            /* turns out this abbr is too long for our liking, so 
               increase i to cut it out. If this is causing errors
               the limit could probably just be increased.*/
            if (++this_abbr_len > 2) {
                printf("Abbr '%s' too long\n", &s[i]);
                i += this_abbr_len;
                break;
            }
        } else {
            // we've hit a lowercase or non-space
            //printf("Ending abbrs on char '%c'\n", s[i]);
            i += 1;
            break;
        }
    }
    if (i<0) {
        i=0;
    }
    
    //printf("i=%d\n", i);
    // i should now point at the start of the abbreviations.
    
    item->name = calloc(i+1, sizeof(char));
    item->dietary_indicators = calloc(n-i+1, sizeof(char));
    
    if (!item->name || !item->dietary_indicators) {
        perror("parse_menu_item: failed to allocate memory");
        return 0;
    }
    
    item->name = strip_ncopy(s, i);
    item->dietary_indicators = strip_ncopy(&s[i], n-i);
    
    if (i < 3) {
        printf("(\"%s\", \"%s\") seems to be missing a meal name (input string was \"%s\"). i: %d, n-i: %d \n", item->name, item->dietary_indicators, s, i, n-i);
    }
    
    return 1;
}

void json_write_menu_item(FILE *f, menu_item *i) {
    /* This is couple, where the first is the item
     * name, and the second is the dietary indicators. */
    fputs("[", f);
    json_write_string_value(f, i->name);
    fputs(", ",f );
    json_write_dietary_list(f, i->dietary_indicators);
    fputs("]", f);
}

void json_write_single_page_paras (FILE *f, struct page *page) {
    /* if this is the first paragraph then don't print a separator */
    bool first = true;
    gchar *elemtext_enc;

    for (int i=0; i<page->n_paragraphs; i++) {
        if (first)
            first = false;
        else
            fputs(", ", f);
        
        fputc('"', f);
        elemtext_enc = json_str_encode(page->paragraphs[i]);
        fputs(elemtext_enc, f);
        free(elemtext_enc);
        fputc('"', f);
    }
}

void json_write_paras_by_page (FILE *f, struct page *pages, int n_pages)
{
    /* if this is the first page then don't print a separator */
    bool first = true;
    
    for (int i=0; i<n_pages; i++) {
        if (first)
            first = false;
        else
            fputs(",\n", f);
        
        fputc('[', f);
        json_write_single_page_paras(f, &pages[i]);
        fputc(']', f);
    }
}
