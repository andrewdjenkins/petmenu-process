/*
    menu2json -  Converts the Peterhouse hall menu to a json document.

    Copyright (C) 2020  Andrew Jenkins

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 dated June, 1991.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <libgen.h>

#include <poppler.h>
#include <glib.h>
#include "hallmenu_common.h"

void add_text_from_elem(FILE *f, PopplerStructureElement *elem) {

    char *text, *text2;

    text = poppler_structure_element_get_text(elem,
        POPPLER_STRUCTURE_GET_TEXT_NONE);

    // i.e. if we got a pointer AND got a non-empty string
    if (text && text[0]) {
	/* IF A NEW VERSION OF POPPLER FIXES THE ENCODING ISSUE, COMMENT OUT
	* THE NEXT THREE LINES */
    	text2 = pdfdocencoding_workaround_copy(text);
	g_free(text);
    	text = text2; 
        
	text2 = json_str_encode(text);
        g_free(text);
        fputs(text2, f);
        free(text2);
        // paragraphs should end in a newline, but currently all content does.
        if (poppler_structure_element_get_kind(elem)
                == POPPLER_STRUCTURE_ELEMENT_CONTENT ) {
            fputs("\\n", f);
        }
    }

}

void iter_add_text_recursive(FILE *f, PopplerStructureElementIter *iter) {

    do {
    PopplerStructureElement *elem =
        poppler_structure_element_iter_get_element(iter);

    add_text_from_elem(f, elem);

    PopplerStructureElementIter *child =
        poppler_structure_element_iter_get_child (iter);
    if (child) iter_add_text_recursive (f, child);
    poppler_structure_element_iter_free (child);
    } while (poppler_structure_element_iter_next (iter));
}

void json_write_structure_tables (FILE *f, PopplerStructureElementIter *iter)
{
    /* if this is the first item of interest at this particular depth then don't
       print a separator */
    static int nrows = 0;
    static int ntabs = 0;
    bool first = true;
    do {
        PopplerStructureElement *elem =
            poppler_structure_element_iter_get_element(iter);
        PopplerStructureElementKind kind =
            poppler_structure_element_get_kind(elem);

        PopplerStructureElementIter *child =
            poppler_structure_element_iter_get_child (iter);

        bool recurse = true;
        switch (kind) {
            case POPPLER_STRUCTURE_ELEMENT_TABLE:
                if (first) {
                    first = false;
                } else {
                    fputs(", ", f);
                }
                fputs("[", f);
                break;
            case POPPLER_STRUCTURE_ELEMENT_TABLE_ROW:
                if (first) {
                    first = false;
                } else {
                    fputs(", ", f);
                }
                fputs("[", f);
                break;
            case POPPLER_STRUCTURE_ELEMENT_TABLE_DATA:
                if (first) {
                    first = false;
                } else {
                    fputs(", ", f);
                }
                fputc('"', f);
                iter_add_text_recursive(f, child);
                fputc('"', f);
                recurse = false;
                break;
            default:; // do nothing for other kinds of element.
        }

        if (recurse) {
            if (child) json_write_structure_tables (f, child);
            poppler_structure_element_iter_free (child);
        }

        switch (kind) {
            case POPPLER_STRUCTURE_ELEMENT_TABLE:
                fputs("]\n", f);
                ntabs++;
                break;
            case POPPLER_STRUCTURE_ELEMENT_TABLE_ROW:
                fputs("]\n", f);
                fprintf(stderr, "\rExtracting: %d tables; %d rows", ntabs, ++nrows);
                break;
            default:; // do nothing for other kinds of element.
        }

    } while (poppler_structure_element_iter_next (iter));
    fprintf(stderr, "\r");
}

void json_write_structure_toplevel_paras (FILE *f, PopplerStructureElementIter *iter)
{
    /* if this is the first paragraph then don't print a separator */
    bool first = true;
    gchar *elemtext;
    gchar *elemtext_enc;

    do {
        PopplerStructureElement *elem =
            poppler_structure_element_iter_get_element(iter);
        PopplerStructureElementKind kind =
            poppler_structure_element_get_kind(elem);

        if (kind == POPPLER_STRUCTURE_ELEMENT_PARAGRAPH) {
            if (first)
                first = false;
            else
                fputs(", ", f);

            fputc('"', f);
            elemtext = poppler_structure_element_get_text(elem,
                        POPPLER_STRUCTURE_GET_TEXT_RECURSIVE);
	    /* IF A NEW VERSION OF POPPLER FIXES THE ENCODING ISSUE, COMMENT OUT
	     * THE NEXT THREE LINES */
	    elemtext_enc = pdfdocencoding_workaround_copy(elemtext);
            g_free(elemtext);
	    elemtext = elemtext_enc;

            elemtext_enc = json_str_encode(elemtext);
	    g_free(elemtext);
            fputs(elemtext_enc, f);
            free(elemtext_enc);
            fputc('"', f);
        }
    } while (poppler_structure_element_iter_next (iter));
}


int main(int argc, char** argv) {
    char *outfile;
    bool print_to_stdout = false;

    if (argc < 2 || argc > 3) {
        puts("usage: ./pdf2html FILENAME [OUTFILE]\n"
             "    Using '-' for OUTFILE, or not providing an argument, will\n"
             "    cause the program to print to stdout.");
        return 1;
    }

    if (argc == 3) {
        outfile = argv[2];
        print_to_stdout = (strcmp(outfile, "-") == 0);
    } else {
        print_to_stdout = true;
    }

    char *abspath = realpath(argv[1], NULL);
    if (!abspath) {
        printf("Failed to determine absolute path of %s", argv[1]);
        return 1;
    }
    gchar *pdf_uri = g_filename_to_uri(abspath, NULL, NULL);
    if (!pdf_uri) {
        puts("Could not create uri.");
        free(abspath);
        return 1;
    }
    PopplerDocument *my_document =
        poppler_document_new_from_file(pdf_uri, NULL, NULL);

    if (!my_document) {
        printf("Could not open document.\n");
        g_free(pdf_uri);
        free(abspath);
        return 1;
    }

    if (!print_to_stdout) {
        puts("Document info:");
        print_document_metadata(my_document, "    ");
    }

    PopplerStructureElementIter *struct_elems =
        poppler_structure_element_iter_new(my_document);

    if (!struct_elems) {
        puts("Parsing cannot continue: document contains no structure elements.");
        return 1;
    }

    FILE *f = print_to_stdout ? stdout : fopen(outfile, "w");
    fputs("{ \"meta\" : {\n", f);

    char *basename_enc = json_str_encode(basename(abspath));
    free(abspath);
    fprintf(f, "\"filename\" : \"%s\",\n", basename_enc);
    free(basename_enc);
    json_write_document_metadata(f, my_document);
    fputs("},\n\"tables\" : [", f);
    PopplerStructureElementIter *tl = // the top-level document part
        poppler_structure_element_iter_get_child(struct_elems);
    json_write_structure_tables(f, struct_elems);
    poppler_structure_element_iter_free(struct_elems);
    fputs("],\n\"paragraphs\" : [", f);
    json_write_structure_toplevel_paras(f, tl);
    poppler_structure_element_iter_free(tl);
    fputs("]}\n", f);
    fclose(f);

    g_free(my_document);
    g_free(pdf_uri);
    return 0;
}
