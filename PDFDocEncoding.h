//========================================================================
//
// PDFDocEncoding.cc
//
// Copyright 2002-2003 Glyph & Cog, LLC
//
//========================================================================

//========================================================================
//
// Modified under the Poppler project - http://poppler.freedesktop.org
//
// All changes made under the Poppler project to this file are licensed
// under GPL version 2 or later
//
// Copyright (C) 2008 Michael Vrable <mvrable@cs.ucsd.edu>
// Copyright (C) 2019 Volker Krause <vkrause@kde.org>
//
// To see a description of the changes please see the Changelog file that
// came with your tarball or type make ChangeLog if you are building from git
//
//========================================================================

//========================================================================
// Modified further by Andrew Jenkins for hallmenu
//========================================================================

#ifndef PDFDocEncoding_h
#define PDFDocEncoding_h

#include <stdint.h>

extern const uint32_t pdfDocEncoding[256];

#endif
