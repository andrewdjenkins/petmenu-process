# petmenu-process

A collection of programs to convert a Peterhouse menu PDF to usable JSON data.

## Installation
1. Build the application using `make`. This requires `poppler-glib`, its
   development headers (i.e. `poppler-glib-dev` or something to that effect),
   and `pkg-config`. If you're installing on the SRCF, these should already exist.
1. Install the application using `make install`. To install without root
   privileges (which is probably a sensible idea), use `PREFIX=~/.local make install`.
1. Install the petmenu Python package. The most sensible way to do this for
   development is to install in editable mode:
	`python3 -m pip install -e python-petmenu/`

   Another option (which requires the `build` Python package) is to build and install the package.
	1. `cd python-petmenu`
	1. `python3 -m build`
	1. `python3 -m pip install dist/<package name>.tar.gz`
   This takes longer and you will need to rebuild and reinstall for changes you
   make to petmenu.py to take effect.

## Building on Windows (MSYS2)
1. Install [https://www.msys2.org/](MSYS2) - this is a requirement for building the programs.
1. I used the `UCRT64` envirnoment. In the MSYS UCRT64 prompt, install
`base-devel` and `mingw-w64-ucrt-x86_64-toolchain` for the compiler/linker/`make`,
and install dependencies `mingw-w64-ucrt-x86_64-poppler` and `mingw-w64-ucrt-x86_64-iconv`.
Something like: `pacman -Syu && pacman -S base-devel mingw-w64-ucrt-x86_64-toolchain mingw-w64-ucrt-x86_64-poppler mingw-w64-ucrt-x86_64-iconv`
1. Change to the source dir and run `make` to build the programs,
and optionally `make install` so they can be used from any folder while in the MSYS prompt.
They can now be used in the same way as usual (see below) within the MSYS UCRT64 prompt.
1. If you the programs to be able to run in a normal command prompt rather than just the
MSYS2 UCRT prompt, it will need to find the DLLs for its dependencies. Add `C:/msys64/ucrt64/bin/`
(and `C:/msys64/usr/local/bin` if you did `make install`) to the `PATH` environment variable
before invoking the program (e.g., in PowerShell: `$Env:Path += ';C:\msys64\ucrt64\bin;C:\msys64\usr\local\bin'; menu2json.exe PATH_TO_MENU OUTPUT_FILENAME`).
The other, more tedious option is to copy the 24 dependency DLLs from the `bin` folder to
the same folder as the program.

## Building on other platforms
If building on Haiku (and possibly other operating systems), you may need to link against
libiconv if it is not builtin to glibc. Invoke `make` with `EXTRA_LDLIBS="-liconv" make`.
(This environment variable is not needed on Windows, because `-liconv` is added by the Makefile if Windows is detected)


## Usage:
1. Obtain the menu PDFs.
1. For servery/brunch menus:
	1. Extract the raw tables with `menu2json PDF OUTPUT_JSON` or `brunch2json PDF OUTPUT_JSON`
	1. Reformat the tables with `python3 -m petmenu reformat INPUT_JSON1 INPUT_JSON2 servery.json`
1. For formal menus, extract the final formatted json directly with `formal2json PDF formal.json`

## License:
The programs `menu2json`, `brunch2json` and `formal2json` are released under the GNU General Public License version
2 only (file `COPYING`) as they use the `poppler-glib` library.

The Python programs & libraries here (viz. pdfdocenc.py and the contents of `python-petmenu/`)
are released under the GNU General Public License version 2, or (at your
option) any later version.
