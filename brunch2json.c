/*
    brunch2json -  Converts the Peterhouse brunch menu to a json document.

    Copyright (C) 2024  Flórián Tiefenbeck
    Some formatting changes and additional annotation by Andrew Jenkins

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 dated June, 1991.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <libgen.h>

#include <poppler.h>
#include <glib.h>
#include "hallmenu_common.h"

/* Functions json_write_structure_tables and json_write_structure_toplevel_paras
 * are called this not because it's what they actually do, but because their
 * output is intended to emulate the output of menu2json when it was used on
 * the brunch menus from before Michaelmas 2024 */

bool isDietaryInfo(const char* text) {
    /*
    *  This is needed to detect if an element contains only the dietary information,
    *  this can happen if the dish has a long name and the dietary info is on the next line in the PDF
    *
    *   Assumption: the dietary info only contains uppercase letters, spaces and endlines and contains at least one uppercase letter
    */

    bool dietary = false;

    while(*text != '\0')
    {
        /* we have found a non-uppercase, non-space, non-newline text character */
        if(*text != ' ' && *text != '\n' && (*text <'A' || *text > 'Z'))
        {
           return false;
        }
        if(*text >'A' && *text < 'Z')
        {
           dietary = true;
        }
        text++;
    }

    return dietary;
}


void add_text_from_elem(FILE *f, PopplerStructureElement *elem, uint8_t endlines) {

    char *text, *text2;

    char *lastFour = malloc(4); /* Stores the last four characters in the file*/

    text = poppler_structure_element_get_text(elem,
        POPPLER_STRUCTURE_GET_TEXT_NONE);

    // i.e. if we got a pointer AND got a non-empty string
    if (text && text[0]) {
	/* IF A NEW VERSION OF POPPLER FIXES THE ENCODING ISSUE, COMMENT OUT
	* THE NEXT THREE LINES */
    	text2 = pdfdocencoding_workaround_copy(text);
	    g_free(text);
    	text = text2;

        /* Now replace the text with itself encoded as for JSON */
	    text2 = json_str_encode(text);
        g_free(text); /*TODO: is g_free the right one to use here? */

        if(isDietaryInfo(text2))
        {
            /*  The current element only has dietary info, so remove the newlines inserted
             *  after the previous item so the python script knows this is the dietary info
             *  and not a separate dish
            */

            fseek(f, -4, SEEK_END ); /*check the last four characters, I think the if is always entered*/

            lastFour[0] = fgetc(f);
            lastFour[1] = fgetc(f);
            lastFour[2] = fgetc(f);
            lastFour[3] = fgetc(f);

            if(strcmp(lastFour, "\\n\\n"))
            {
               fseek(f, -4, SEEK_END ); // move the pointer back, so the \n-s get overwritten
            }
        }

        fputs(text2, f);
        free(text2);
        free(lastFour);

        while(endlines>0)
        {
            endlines--;
            fputs("\\n", f);
        }

    }

}

int8_t pageNumber = -1;
void json_write_structure_tables (FILE *f, PopplerStructureElementIter *iter, int level) {
    /* recursively go through the PDF to find the text we need
    *  the recursion level is utilised to differentiate dates and dishes */
    do{
        PopplerStructureElement *elem =
            poppler_structure_element_iter_get_element(iter);

        /* only the CONTENT element kind stores text, so get the text if this
        element is one of them*/
        if(poppler_structure_element_get_kind(elem) == POPPLER_STRUCTURE_ELEMENT_CONTENT){
            /* Check if we're on a new page */
            if(pageNumber != poppler_structure_element_get_page(elem)) {
                pageNumber = poppler_structure_element_get_page(elem);
                if(pageNumber == 0) /* Start the list if this is the first page */
                    fputs("[", f);
                else /* close the previous page if this is not the first page*/
                    fputs("\"]],\n ",f);

                fputs("[[\"",f);
                /* The date is in the first content element of the page */
                add_text_from_elem(f, elem,1);
                fputs("\"], \n[\"",f);
            } else if(level==3) {
                /* the dishes are on recursion level 3 on each page */
                add_text_from_elem(f, elem,2);
            }
        }

        /* Now try to make an iterator over the children of this element */
        PopplerStructureElementIter *child =
            poppler_structure_element_iter_get_child (iter);
        /* If we get an iterator, then this element has child elements so we
         * recurse to the next level down to get the text from them */
        if (child)
            json_write_structure_tables(f, child, level+1);
        poppler_structure_element_iter_free(child);

    }while(poppler_structure_element_iter_next (iter));

    fprintf(stderr, "\r");
}

void json_write_structure_toplevel_paras (FILE *f, PopplerStructureElementIter *iter, int level) {
    /* recursively go through the PDF to find the dietary abbreviations and
     * their explanations (like "V - Vegetarian")
     * the recursion level is utilised to find it */
    do{
        PopplerStructureElement *elem =
            poppler_structure_element_iter_get_element(iter);

        if(poppler_structure_element_get_kind(elem) == POPPLER_STRUCTURE_ELEMENT_CONTENT){
            /* assumption: the dietary info is on the 2nd level
             * and it's not the first 2nd level item on a page (that's the date)
             * (note: The global variable pageNumber is shared with json_write_structure_tables)*/
            if (pageNumber != poppler_structure_element_get_page(elem))
                pageNumber = poppler_structure_element_get_page(elem);
            else if(level==2)
                add_text_from_elem(f, elem,0);
        }
        /* Now iterate recursively over the structure as before */
        PopplerStructureElementIter *child =
        poppler_structure_element_iter_get_child (iter);
            if(child)
            json_write_structure_toplevel_paras(f, child, level+1);
        poppler_structure_element_iter_free(child);

    }while(poppler_structure_element_iter_next (iter));

}


int main(int argc, char** argv) {
    char *outfile;
    bool print_to_stdout = false;
    if (argc < 2 || argc > 3) {
        puts("usage: ./brunch2json FILENAME [OUTFILE]\n"
            "    Using '-' for OUTFILE, or not providing an argument, will\n"
            "    cause the program to print to stdout.\n\n"
            "    This program is for brunch menus from Michaelmas 2024 or later.\n"
            "    Use ./menu2json for older menus.\n");
        return 1;
    }

    if (argc == 3) {
        outfile = argv[2];
        print_to_stdout = (strcmp(outfile, "-") == 0);
    } else {
        print_to_stdout = true;
    }

    char *abspath = realpath(argv[1], NULL);
    if (!abspath) {
        printf("Failed to determine absolute path of %s", argv[1]);
        return 1;
    }
    gchar *pdf_uri = g_filename_to_uri(abspath, NULL, NULL);
    if (!pdf_uri) {
        puts("Could not create uri.");
        free(abspath);
        return 1;
    }
    PopplerDocument *my_document =
        poppler_document_new_from_file(pdf_uri, NULL, NULL);

    if (!my_document) {
        printf("Could not open document.\n");
        g_free(pdf_uri);
        free(abspath);
        return 1;
    }

    if (!print_to_stdout) {
        puts("Document info:");
        print_document_metadata(my_document, "    ");
    }
    /* One copy is for getting the date and menu items, and the other for the
     * dietary information */
    PopplerStructureElementIter *struct_elems =
        poppler_structure_element_iter_new(my_document);
    PopplerStructureElementIter *struct_elems2 =
        poppler_structure_element_iter_new(my_document);
    if (!struct_elems) {
        puts("Parsing cannot continue: document contains no structure elements.");
        return 1;
    }

    FILE *f = print_to_stdout ? stdout : fopen(outfile, "w+");

    fputs("{ \"meta\" : {\n", f);

    char *basename_enc = json_str_encode(basename(abspath));
    free(abspath);
    fprintf(f, "\"filename\" : \"%s\",\n", basename_enc);
    free(basename_enc);
    json_write_document_metadata(f, my_document);
    fputs("},\n\"tables\" : ", f);
    json_write_structure_tables(f, struct_elems, 0);
    poppler_structure_element_iter_free(struct_elems);
    fputs("\"]]],\n\"paragraphs\" : [\"", f);
    json_write_structure_toplevel_paras(f, struct_elems2,0);
    poppler_structure_element_iter_free(struct_elems2);
    fputs("\"]}\n", f);
    fclose(f);

    g_free(my_document);
    g_free(pdf_uri);
    return 0;
}
