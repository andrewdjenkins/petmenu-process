CC=gcc
CFLAGS=`pkg-config --cflags poppler-glib` -Wall -Wextra $(EXTRA_CFLAGS)
LDLIBS=`pkg-config --libs poppler-glib` $(EXTRA_LDLIBS)

#install bit inspired by https://stackoverflow.com/a/39895302
ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

# https://stackoverflow.com/questions/714100/os-detecting-makefile#12099167
# Link against iconv if building on Windows.
ifeq ($(OS),Windows_NT)
	LDLIBS += -liconv
endif

.PHONY : clean all install uninstall

all : menu2json formal2json brunch2json

menu2json : menu2json.c hallmenu_common.c PDFDocEncoding.c
	$(CC) $(CFLAGS) menu2json.c hallmenu_common.c PDFDocEncoding.c $(LDLIBS) -o menu2json

formal2json : formal2json.c hallmenu_common.c PDFDocEncoding.c
	$(CC) $(CFLAGS) formal2json.c hallmenu_common.c PDFDocEncoding.c $(LDLIBS) -o formal2json 

brunch2json : brunch2json.c hallmenu_common.c PDFDocEncoding.c
	$(CC) $(CFLAGS) brunch2json.c hallmenu_common.c PDFDocEncoding.c $(LDLIBS) -o brunch2json


install : menu2json formal2json brunch2json
	install -d $(DESTDIR)$(PREFIX)/bin/
	install menu2json formal2json brunch2json $(DESTDIR)$(PREFIX)/bin/

uninstall :
	rm -f $(DESTDIR)$(PREFIX)/bin/menu2json $(DESTDIR)$(PREFIX)/bin/formal2json $(DESTDIR)$(PREFIX)/bin/brunch2json

clean :
	rm -f *.o
	rm -f menu2json
	rm -f formal2json
	rm -f brunch2json
