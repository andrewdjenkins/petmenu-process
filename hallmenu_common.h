/*
    hallmenu_common.h -  Functions useful to both formal and regular menus.

    Copyright (C) 2020-21  Andrew Jenkins

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 dated June, 1991.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef HALLMENU_COMMON_H
#define HALLMENU_COMMON_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <errno.h>
#include <libgen.h>
#include <limits.h>

/* iconv is there to work around poppler's encoding issue */
#include <iconv.h>
#include <poppler.h>
#include <glib.h>
#include "PDFDocEncoding.h"

/* In order to allow building on Windows */
#ifdef __WIN32__
    #define realpath(rel, abs) _fullpath(abs, rel, PATH_MAX)
#endif

/* Poppler/meta stuff */
void print_poppler_info(void);
void print_document_metadata(PopplerDocument *d, const char *indent);

/*
    Step 1 is to use Poppler to get the text out of all the paragraphs
    on each page. This is a nice way to hold paragraphs in a page
*/
#define MAX_PARAGRAPHS 512
struct page {
    int pagenum;
    int n_paragraphs;
    gchar* paragraphs[MAX_PARAGRAPHS];
};
/* add_paragraph will add a reference to paragraph s within the page
 * struct, returning 1 on success and 0 on failure.
 *
 * free_paragraphs will free all of the pointers referred to in the page
 * struct.*/
int page_add_paragraph(struct page *page, gchar *s);
void page_free_paragraphs(struct page *page);

/* Load the paragraphs from the page to which the iterator is currently pointing
 * into the struct */
void structure_load_page_text(struct page *pages, PopplerStructureElementIter *iter);


/* String stuff */
/* checks that the values are valid unicode codepoints. May help to diagnose errors if run on a
machine of different endianness? equally it may not */
bool chk_codepoints(uint32_t *input, size_t n_input);

/* Where `date` is a date of the form "Tuesday 2nd January 2021"
 * and `dest` is a buffer at least 11 chars large, this will process the date
 * into a form resembling "2021-01-19"*/
int parse_date(char *date, char *dest);
/* Given a date in the form 2020-01-10 and a buffer at least 16 chars long,
 * will write a human-readable name of the term starting near that date, e.g.
 * 2020-01-10 -> Lent 2020. This function will null-terminate its output.
 */
void deduce_term_name(char *dest, const char *date);

/* This function adds to_add to *dest after a space.
 * It does this by realloc'ing *dest, so it MUST be a string in the heap.
 * May return NULL on failure. */
char* space_join_replace(char** dest, char* to_add);

/* Returns a malloc'd copy of input with leading and trailing spaces removed
 * ncopy does the same except it writes at most n chars, NOT INCLUDING the
 * null terminator which it WILL write. (thus the buffer needs at least n+1) */
char* strip_copy(const char *input);
char* strip_ncopy(const char *input, size_t n);
/* take a wild guess what this one tells you */
bool all_spaces(const char *input);
bool is_lowercase(char c);

/* Encoding stuff */

/* 
    This exists as workaround for an encoding bug in poppler.
    
    Rather like iconv, this function reads at most n_input code points
    from the input, and writes at most n_output pdfdocencoding-encoded
    chars to the output buffer. 
    input must be a list of Unicode code points (utf32 or whatever).
*/
extern int pdfdocencoding_lut_hits;
extern int pdfdocencoding_lut_misses;
void pdfdocencoding_encode(uint32_t *input, size_t n_input,
                          char *output, size_t n_output);
/* this rather non-obvious function takes a string as returned by
 * Poppler and returns a freshly glib-alloc'd UTF8 string. */
gchar *pdfdocencoding_workaround_copy(char *s);


/* Menu stuff */

/* This is a (name, dietary info) couple like in the json */
typedef struct _menu_item {
    char* name;
    char* dietary_indicators;
} menu_item;

/* Parse the combined string `s` into a name and a set of indicators.
 * Indicators are taken to be allcaps strings of no more than three
 * characters.
 * Allocates two fresh strings and puts a reference to them in `item` */
int parse_menu_item(char *s, menu_item *item);


/* Generic json stuff */
/* Allocate a new string that can be written to a json file within quotes
 * Does things like escape newlines and special chars. */
gchar *json_str_encode(const gchar *s);
/* these functions write "$k" : and "$k": "$v" respectively, saving the need
 * to json_str_encode by hand each time. */
void json_write_key(FILE *f, char *k);
void json_write_string_value(FILE *f, char *v);
/* note that if v is the null pointer then these will write NULL instead of
 * trying to dereference it and crashing. */
void json_write_string_k_v(FILE *f, char *k, char*v);
void json_write_long_k_v(FILE *f, char *k, long int i);
void json_write_null_k_v(FILE *f, char *k);

/* Specific json stuff */
void json_write_document_metadata(FILE *f, PopplerDocument *d);
/* Given a space-separated string of dietary requirement abbreviations, will
 * write them to file as a json list. e.g. "VV G D" -> ["VV", "G", "D"] */
void json_write_dietary_list(FILE *f, char *l);
/* Write a (item_name, [requirement abbrs]) couple to file */
void json_write_menu_item(FILE *f, menu_item *i);
/* write_single_page_paras writes a list containing all of the paragraphs
 * from a single page. write_paras_by_page writes a list containing multiple
 * such single-page lists */
void json_write_single_page_paras(FILE *f, struct page *page);
void json_write_paras_by_page(FILE *f, struct page *pages, int n_pages);

#endif /* HALLMENU_COMMON_H */
